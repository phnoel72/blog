class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  validates_presence_of :title
  validates_presence_of :body
#  validates_presence_of :name
# validates_numercality_of :arg, :only_integer => true
# validates_confirmation_of :email
# validates_length_of :password, :in => 8..20
end
